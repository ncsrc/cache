package dev.test.cache;

import dev.test.cache.config.CacheConfiguration;
import dev.test.cache.config.CachingStrategy;
import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.config.ram.RamConfiguration;


public class MockConfiguration {


	public static CacheConfiguration getCacheConfiguration(final CachingStrategy cachingStrategy) {
		return new CacheConfiguration(getRamConfiguration(), getFileSystemConfiguration(), cachingStrategy);
	}


	public static RamConfiguration getRamConfiguration() {
		return new RamConfiguration(2, EvictionPolicy.FIFO);
	}


	public static FileSystemConfiguration getFileSystemConfiguration() {
		return new FileSystemConfiguration("cache_test", 1, SizeUnit.KB, false, EvictionPolicy.FIFO, 3);
	}


}

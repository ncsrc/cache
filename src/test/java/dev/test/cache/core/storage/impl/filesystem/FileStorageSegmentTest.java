package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.core.serializer.Serializer;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


// More likely IT, since mocks for inner streams aren't used
public class FileStorageSegmentTest {

	private FileStorageSegment segment;
	private FileSystemConfiguration mockConf;
	private Path segmentLocation;


	@Before
	public void setUp() throws Exception {
		mockConf = new FileSystemConfiguration(".", 1, SizeUnit.KB, false, EvictionPolicy.FIFO, 3);
		segmentLocation = Paths.get(getClass().getClassLoader().getResource("cache_test").getPath());
		segment = new FileStorageSegment(segmentLocation, mockConf);
	}


	@After
	public void tearDown() throws Exception {
		segment.close();
	}



	@Test
	public void when_locked_then_tryLock_is_false() throws Exception {
		final boolean firstTry = segment.tryLock();

		new Thread(() -> {

			final boolean secondTry = segment.tryLock();
			assertThat(secondTry).isFalse();

		}).start();

		assertThat(firstTry).isTrue();
	}


	@Test
	public void when_unlocked_then_tryLock_is_true() throws Exception {
		final boolean firstTry = segment.tryLock();
		segment.unlock();

		new Thread(() -> {

			final boolean secondTry = segment.tryLock();
			assertThat(secondTry).isTrue();

		}).start();

		assertThat(firstTry).isTrue();
	}


	@Test
	public void when_currentsize_is_equal_to_max_amount_then_isFull_true() throws Exception {
		final FileSystemConfiguration configuration = new FileSystemConfiguration(
				".", 1, SizeUnit.KB,
				false, EvictionPolicy.FIFO,
				100000
		);

		segment.close();
		segment = new FileStorageSegment(segmentLocation, configuration);
		final boolean full = segment.isFull();

		assertThat(full).isTrue();
	}


	@Test
	public void when_currentsize_is_less_then_max_amount_then_isFull_false() throws Exception {
		final boolean full = segment.isFull();
		assertThat(full).isFalse();
	}


	@Test
	public void when_single_write_then_read_returns_correct_value() throws Exception {
		final byte[] initalKey = Serializer.serialize("mock-key");
		final byte[] initialValue = Serializer.serialize("mock-value");

		segment.write(initalKey, initialValue);
		final byte[] gotValue = segment.get(initalKey);

		assertThat(Arrays.equals(gotValue, initialValue)).isTrue();
		assertThat(gotValue).isEqualTo(initialValue);
	}


	@Test
	public void when_multiple_writes_then_read_returns_correct_value_of_the_middle_key() throws Exception {
		final List<byte[]> keys = getMockKeys();
		final List<byte[]> values = getMockValues();

		writeMocks(keys, values);

		final byte[] gotValue = segment.get(keys.get(1));
		assertThat(gotValue).isEqualTo(values.get(1));
	}

	@Test
	public void when_key_not_found_then_get_returns_null() throws Exception {
		final byte[] bytes = segment.get(Serializer.serialize("non-existent"));
		assertThat(bytes).isNull();
	}


	@Test
	public void when_replace_existing_key_then_true_and_read_returns_replaced_correctly() throws Exception {
		final byte[] key = Serializer.serialize("key");
		final byte[] originalValue = Serializer.serialize("value-original");
		final byte[] replacement = Serializer.serialize("value-replacement");

		segment.write(key, originalValue);
		final boolean replaced = segment.replace(key, replacement);
		final byte[] newValue = segment.get(key);

		assertThat(replaced).isTrue();
		assertThat(newValue).isEqualTo(replacement);
	}


	@Test
	public void after_replace_file_is_not_corrupted_and_reads_successfully() throws Exception {
		final List<byte[]> keys = getMockKeys();
		final List<byte[]> values = getMockValues();
		writeMocks(keys, values);

		final byte[] replacement = Serializer.serialize("replacement");
		segment.replace(keys.get(1), replacement);

		final byte[] firstValue = segment.get(keys.get(0));
		final byte[] secondValue = segment.get(keys.get(1));
		final byte[] thirdValue = segment.get(keys.get(2));

		assertThat(firstValue).isEqualTo(values.get(0));
		assertThat(secondValue).isEqualTo(replacement);
		assertThat(thirdValue).isEqualTo(values.get(2));
	}


	@Test
	public void when_key_is_not_in_file_then_replace_returns_false() throws Exception {
		final boolean replaced = segment.replace(Serializer.serialize("key"), Serializer.serialize("value"));
		assertThat(replaced).isFalse();
	}


	@Test
	public void when_remove_existing_then_true_is_returned() throws Exception {
		final List<byte[]> mockKeys = getMockKeys();
		final List<byte[]> mockValues = getMockValues();

		segment.write(mockKeys.get(0), mockValues.get(0));
		final boolean removed = segment.remove(mockKeys.get(0));

		assertThat(removed).isTrue();
	}


	@Test
	public void when_remove_non_existing_then_false_is_returned() throws Exception {
		final boolean removed = segment.remove(Serializer.serialize("key"));
		assertThat(removed).isFalse();
	}


	@Test
	public void remove_does_not_corrupts_the_file() throws Exception {
		final List<byte[]> mockKeys = getMockKeys();
		final List<byte[]> mockValues = getMockValues();
		writeMocks(mockKeys, mockValues);

		final boolean removed = segment.remove(mockKeys.get(1));

		final byte[] firstValue = segment.get(mockKeys.get(0));
		final byte[] secondValue = segment.get(mockKeys.get(1));
		final byte[] thirdValue = segment.get(mockKeys.get(2));

		assertThat(removed).isTrue();
		assertThat(firstValue).isEqualTo(mockValues.get(0));
		assertThat(secondValue).isNull();

		assertThat(thirdValue).isEqualTo(mockValues.get(2));
	}


	@Test
	public void after_clear_previously_wrote_key_not_found() throws Exception {
		final List<byte[]> mockKeys = getMockKeys();
		final List<byte[]> mockValues = getMockValues();
		writeMocks(mockKeys, mockValues);

		segment.clear();
		final boolean contains = segment.contains(mockKeys.get(2));

		assertThat(contains).isFalse();
	}


	@Test
	public void after_write_contains_returns_true() throws Exception {
		final byte[] key = Serializer.serialize("key");
		final byte[] value = Serializer.serialize("value");
		segment.write(key, value);

		final boolean contains = segment.contains(key);
		assertThat(contains).isTrue();
	}


	@Test
	public void when_key_is_not_in_file_contains_returns_false() throws Exception {
		final byte[] key = Serializer.serialize("key");
		final boolean contains = segment.contains(key);
		assertThat(contains).isFalse();
	}


	@Test
	public void when_non_persistent_close_removes_file() throws Exception {
		segment.close();
		final File[] files = this.segmentLocation.toFile().listFiles();

		// to prevent exception from tearDown
		segment = new FileStorageSegment(this.segmentLocation, mockConf);

		// nothing except stub file
		assertThat(files.length).isEqualTo(1);
	}


	private List<byte[]> getMockValues() {
		return Arrays.asList(
				Serializer.serialize("mock-value-1"),
				Serializer.serialize("mock-value-2"),
				Serializer.serialize("mock-value-3")
		);
	}


	private List<byte[]> getMockKeys() {
		return Arrays.asList(
				Serializer.serialize("mock-key-1"),
				Serializer.serialize("mock-key-2"),
				Serializer.serialize("mock-key-3")
		);
	}


	private void writeMocks(List<byte[]> keys, List<byte[]> values) {
		for (int i = 0; i < keys.size(); i++) {
			segment.write(keys.get(i), values.get(i));
		}
	}


}
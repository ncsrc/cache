package dev.test.cache.core.storage.impl.ram;

import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.storage.holders.KeyValuePair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class LruRamStorageTest {

	private final RamConfiguration conf = new RamConfiguration(2, EvictionPolicy.FIFO);
	private LruRamStorage<String, String> storage;

	private KeyValuePair<String, String> pair;


	@Before
	public void setUp() throws Exception {
		storage = new LruRamStorage<>(conf);
		pair = new KeyValuePair<>("key", "value");
	}


	@After
	public void tearDown() throws Exception {

	}



	// save
	@Test
	public void when_save_one_value_then_contains_is_true() throws Exception {
		final KeyValuePair<String, String> evicted = storage.save(pair);

		assertThat(evicted.isPresent()).isFalse();
		assertThat(storage.contains(pair.getKey())).isTrue();
		assertThat(storage.get(pair.getKey())).isEqualTo(pair.getSerializedValue());
		assertThat(storage.keyList.contains(pair.getKey())).isTrue();
	}


	@Test
	public void when_save_in_full_storage_then_evicted_correct_pair() throws Exception {
		final KeyValuePair<String, String> pair2 = new KeyValuePair<>("1", "1");
		final KeyValuePair<String, String> pair3 = new KeyValuePair<>("2", "2");

		final KeyValuePair<String, String> notEvicted1 = storage.save(pair);
		final KeyValuePair<String, String> notEvicted2 = storage.save(pair2);
		storage.get(pair2.getKey());	// to move forward for eviction

		final KeyValuePair<String, String> evicted = storage.save(pair3);

		assertThat(notEvicted1.isPresent()).isFalse();
		assertThat(notEvicted2.isPresent()).isFalse();
		assertThat(evicted.isPresent()).isTrue();
		assertThat(evicted.getSerializedKey()).isEqualTo(pair.getSerializedKey());
		assertThat(evicted.getSerializedValue()).isEqualTo(pair.getSerializedValue());
		assertThat(storage.keyList.contains(pair.getKey())).isFalse();
	}



	// replace
	@Test
	public void when_key_in_storage_then_replace_performed() throws Exception {
		storage.save(pair);
		final KeyValuePair<String, String> replacement = new KeyValuePair<>(pair.getKey(), "replaced");

		final boolean replaced = storage.replace(replacement);

		assertThat(replaced).isTrue();
		assertThat(storage.get(pair.getKey())).isEqualTo(replacement.getSerializedValue());
	}


	@Test
	public void when_key_is_not_in_storage_then_replace_is_false() throws Exception {
		final KeyValuePair<String, String> replacement = new KeyValuePair<>(pair.getKey(), "replaced");
		final boolean replaced = storage.replace(replacement);
		assertThat(replaced).isFalse();
	}



	// get
	@Test
	public void when_get_multiple_times_then_element_swapped() throws Exception {
		final KeyValuePair<String, String> pair2 = new KeyValuePair<>("1", "2");

		storage.save(pair);
		storage.save(pair2);

		storage.get(pair2.getKey());
		storage.get(pair2.getKey());
		storage.get(pair2.getKey());


		assertThat(storage.keyList.get(0)).isEqualTo(pair2.getKey());
		assertThat(storage.keyList.get(1)).isEqualTo(pair.getKey());
	}



	@Test
	public void when_key_is_in_storage_then_get_returns_correct_value() throws Exception {
		storage.save(pair);

		final byte[] value = storage.get(pair.getKey());

		assertThat(value).isEqualTo(pair.getSerializedValue());
	}


	@Test
	public void when_key_is_not_in_storage_then_get_returns_null() throws Exception {
		final byte[] value = storage.get(pair.getKey());
		assertThat(value).isNull();
	}


	// contains
	@Test
	public void when_key_is_in_storage_then_contains_is_true() throws Exception {
		storage.save(pair);
		assertThat(storage.contains(pair.getKey())).isTrue();
	}


	@Test
	public void when_key_is_not_in_storage_then_contains_is_false() throws Exception {
		assertThat(storage.contains(pair.getKey())).isFalse();
	}


	// remove
	@Test
	public void when_remove_then_contains_is_false() throws Exception {
		storage.save(pair);
		final KeyValuePair<String, String> removed = storage.remove(pair.getKey());

		assertThat(removed.getKey()).isEqualTo(pair.getKey());
		assertThat(removed.getSerializedValue()).isEqualTo(pair.getSerializedValue());
		assertThat(storage.contains(pair.getKey())).isFalse();
		assertThat(storage.keyList.contains(pair.getKey())).isFalse();
	}


	@Test
	public void when_remove_non_existent_then_pair_is_null() throws Exception {
		final KeyValuePair<String, String> removed = storage.remove(pair.getKey());
		assertThat(removed).isNull();
	}


	// clear
	@Test
	public void when_clear_then_storage_is_empty() throws Exception {
		storage.save(pair);
		final boolean firstCheck = storage.data.isEmpty();

		storage.clear();
		final boolean secondCheck = storage.data.isEmpty();

		assertThat(firstCheck).isFalse();
		assertThat(secondCheck).isTrue();
		assertThat(storage.keyList.isEmpty()).isTrue();
	}


	// close
	@Test
	public void when_close_then_internals_are_nulls() throws Exception {
		storage.close();

		assertThat(storage.data).isNull();
		assertThat(storage.keyList).isNull();
	}



}
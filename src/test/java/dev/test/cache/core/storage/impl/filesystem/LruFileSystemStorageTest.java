package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.core.storage.holders.KeyValuePair;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class LruFileSystemStorageTest {

	private LruFileSystemStorage<String, String> storage;
	private FileSystemConfiguration mockConf;
	private Path location;

	@Mock
	private FileStorageSegment segment1;

	@Mock
	private FileStorageSegment segment2;

	@Mock
	private FileStorageSegment segment3;

	private final KeyValuePair<String, String> pair = new KeyValuePair<>("key", "value");


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		given(segment1.tryLock()).willReturn(true);
		given(segment2.tryLock()).willReturn(true);
		given(segment3.tryLock()).willReturn(true);

		mockConf = new FileSystemConfiguration("cache_test", 1, SizeUnit.KB, false, EvictionPolicy.FIFO, 3);
		location = Paths.get(getClass().getClassLoader().getResource("cache_test").getPath());
		storage = new LruFileSystemStorage<>(mockConf, "cache_id");
		storage.segments = getSegmentsList();
	}


	@After
	public void tearDown() throws Exception {

	}


	@Test
	public void when_all_segments_are_full_then_eviction_performed() throws Exception {
		given(segment3.contains(pair.getSerializedKey())).willReturn(true);
		storage.fullSegments.addAll(storage.segments);

		// to make it frequently queried
		storage.contains(pair.getSerializedKey());
		storage.contains(pair.getSerializedKey());

		final boolean evicted = storage.save(pair);

		verify(segment2).close();
		assertThat(evicted).isTrue();

		storage.close();
	}






	private List<FileStorageSegment> getSegmentsList() {
		List<FileStorageSegment> list = new LinkedList<>();
		list.add(segment1);
		list.add(segment2);
		list.add(segment3);
		return list;
	}

}
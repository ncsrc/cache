package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemSegmentException;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemStorageException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;


public class FifoFileSystemStorageTest {

	private FifoFileSystemStorage<String, String> storage;
	private FileSystemConfiguration mockConf;
	private Path location;

	@Mock
	private FileStorageSegment segment1;

	@Mock
	private FileStorageSegment segment2;

	@Mock
	private FileStorageSegment segment3;

	private final KeyValuePair<String, String> pair = new KeyValuePair<>("key", "value");


	@Before
	public void setUp() throws Exception {
		initMocks(this);
		given(segment1.tryLock()).willReturn(true);
		given(segment2.tryLock()).willReturn(true);
		given(segment3.tryLock()).willReturn(true);

		mockConf = new FileSystemConfiguration("cache_test", 1, SizeUnit.KB, false, EvictionPolicy.FIFO, 3);
		location = Paths.get(getClass().getClassLoader().getResource("cache_test").getPath());
		storage = new FifoFileSystemStorage<>(mockConf, "cache_id");
		storage.segments = getSegmentsList();
	}

	@After
	public void tearDown() throws Exception {
//		storage.close();
	}


	@Test
	public void when_all_segments_are_full_then_eviction_performed() throws Exception {
		storage.fullSegments.addAll(storage.segments);

		final boolean evicted = storage.save(pair);

		verify(segment1).close();
		assertThat(evicted).isTrue();

		storage.close();
	}


	@Test
	public void when_segments_are_not_full_then_correct_one_is_chosen() throws Exception {
		storage.fullSegments.add(segment1);

		final boolean evicted = storage.save(pair);

		// Chosen segment
		verify(segment2).tryLock();
		verify(segment2).write(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment2).unlock();
		assertThat(evicted).isFalse();
	}


	@Test
	public void when_some_of_segments_are_locked_then_correct_one_is_chosen() throws Exception {
		storage.fullSegments.add(segment1);
		given(segment2.tryLock()).willReturn(false);

		final boolean evicted = storage.save(pair);

		verify(segment2).tryLock();

		// Chosen segment
		verify(segment3).tryLock();
		verify(segment3).write(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment3).unlock();
		assertThat(evicted).isFalse();
	}


	@Test
	public void when_segment_becomes_full_then_it_is_added_to_collection() throws Exception {
		given(segment1.isFull()).willReturn(true);

		final boolean evicted = storage.save(pair);

		// Chosen segment
		verify(segment1).tryLock();
		verify(segment1).write(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment1).unlock();
		assertThat(evicted).isFalse();
		assertThat(storage.fullSegments).contains(segment1);
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_write_is_failed_then_exception() throws Exception {
		doThrow(FileSystemSegmentException.class).when(segment1).write(pair.getSerializedKey(), pair.getSerializedValue());

		storage.save(pair);

		// Chosen segment
		verify(segment1).tryLock();
		verify(segment1).write(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment1).unlock();
	}


	@Test
	public void when_key_is_present_then_replace_performed() throws Exception {
		given(segment1.replace(pair.getSerializedKey(), pair.getSerializedValue())).willReturn(false);
		given(segment2.replace(pair.getSerializedKey(), pair.getSerializedValue())).willReturn(true);

		final boolean replaced = storage.replace(pair);

		assertThat(replaced).isTrue();
		verify(segment1).tryLock();
		verify(segment1).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment1).unlock();
		verify(segment2).tryLock();
		verify(segment2).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment2).unlock();
	}


	@Test
	public void when_key_is_not_present_then_replace_is_not_performed() throws Exception {
		given(segment1.replace(pair.getSerializedKey(), pair.getSerializedValue())).willReturn(false);
		given(segment2.replace(pair.getSerializedKey(), pair.getSerializedValue())).willReturn(false);
		given(segment3.replace(pair.getSerializedKey(), pair.getSerializedValue())).willReturn(false);

		final boolean replaced = storage.replace(pair);

		assertThat(replaced).isFalse();
		verify(segment1).tryLock();
		verify(segment1).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment1).unlock();
		verify(segment2).tryLock();
		verify(segment2).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment2).unlock();
		verify(segment3).tryLock();
		verify(segment3).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_fails_then_exception_thrown() throws Exception {
		given(segment1.replace(pair.getSerializedKey(), pair.getSerializedValue())).willThrow(FileSystemSegmentException.class);
		storage.replace(pair);
		verify(segment1).tryLock();
		verify(segment1).replace(pair.getSerializedKey(), pair.getSerializedValue());
		verify(segment1).unlock();
	}


	@Test
	public void when_key_is_in_middle_segment_then_get_find_it() throws Exception {
		given(segment2.get(pair.getSerializedKey())).willReturn(pair.getSerializedValue());

		final byte[] value = storage.get(pair.getSerializedKey());

		assertThat(value).isEqualTo(pair.getSerializedValue());
		verify(segment1).tryLock();
		verify(segment1).get(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).get(pair.getSerializedKey());
		verify(segment2).unlock();
	}


	@Test
	public void when_key_is_not_present_then_get_returns_null() throws Exception {
		final byte[] value = storage.get(pair.getSerializedKey());

		assertThat(value).isNull();
		verify(segment1).tryLock();
		verify(segment1).get(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).get(pair.getSerializedKey());
		verify(segment2).unlock();

		verify(segment3).tryLock();
		verify(segment3).get(pair.getSerializedKey());
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_fails_then_get_throws_exception() throws Exception {
		given(segment1.get(pair.getSerializedKey())).willThrow(FileSystemSegmentException.class);

		storage.get(pair.getSerializedKey());

		verify(segment1).tryLock();
		verify(segment1).get(pair.getSerializedKey());
		verify(segment1).unlock();
	}



	@Test
	public void when_key_is_in_middle_segment_then_remove_finds_it() throws Exception {
		given(segment2.remove(pair.getSerializedKey())).willReturn(true);

		final boolean removed = storage.remove(pair.getSerializedKey());

		assertThat(removed).isTrue();
		verify(segment1).tryLock();
		verify(segment1).remove(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).remove(pair.getSerializedKey());
		verify(segment2).unlock();
	}


	@Test
	public void when_key_is_not_in_segments_then_remove_returns_false() throws Exception {
		final boolean removed = storage.remove(pair.getSerializedKey());

		assertThat(removed).isFalse();
		verify(segment1).tryLock();
		verify(segment1).remove(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).remove(pair.getSerializedKey());
		verify(segment2).unlock();

		verify(segment3).tryLock();
		verify(segment3).remove(pair.getSerializedKey());
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segments_fails_then_remove_throws_exception() throws Exception {
		given(segment1.remove(pair.getSerializedKey())).willThrow(FileSystemSegmentException.class);
		storage.remove(pair.getSerializedKey());

		verify(segment1).tryLock();
		verify(segment1).remove(pair.getSerializedKey());
		verify(segment1).unlock();
	}



	@Test
	public void when_clear_then_all_segment_cleared() throws Exception {
		storage.clear();

		verify(segment1).tryLock();
		verify(segment1).clear();
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).clear();
		verify(segment2).unlock();

		verify(segment3).tryLock();
		verify(segment3).clear();
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_fails_then_clear_throws_exception() throws Exception {
		doThrow(FileSystemSegmentException.class).when(segment1).clear();

		storage.clear();

		verify(segment1).tryLock();
		verify(segment1).clear();
		verify(segment1).unlock();
	}



	@Test
	public void when_in_middle_segment_then_contains_is_true() throws Exception {
		given(segment2.contains(pair.getSerializedKey())).willReturn(true);

		final boolean actual = storage.contains(pair.getSerializedKey());

		assertThat(actual).isTrue();
		verify(segment1).tryLock();
		verify(segment1).contains(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).contains(pair.getSerializedKey());
		verify(segment2).unlock();
	}


	@Test
	public void when_key_is_not_in_segments_then_contains_is_false() throws Exception {
		final boolean actual = storage.contains(pair.getSerializedKey());

		assertThat(actual).isFalse();
		verify(segment1).tryLock();
		verify(segment1).contains(pair.getSerializedKey());
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).contains(pair.getSerializedKey());
		verify(segment2).unlock();

		verify(segment3).tryLock();
		verify(segment3).contains(pair.getSerializedKey());
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_fails_then_exception() throws Exception {
		given(segment1.contains(pair.getSerializedKey())).willThrow(FileSystemSegmentException.class);
		storage.contains(pair.getSerializedKey());

		verify(segment1).tryLock();
		verify(segment1).contains(pair.getSerializedKey());
		verify(segment1).unlock();
	}



	@Test
	public void when_close_then_all_segments_are_closed() throws Exception {
		storage.close();

		verify(segment1).tryLock();
		verify(segment1).close();
		verify(segment1).unlock();

		verify(segment2).tryLock();
		verify(segment2).close();
		verify(segment2).unlock();

		verify(segment3).tryLock();
		verify(segment3).close();
		verify(segment3).unlock();
	}


	@Test(expected = FileSystemStorageException.class)
	public void when_segment_fails_then_close_throws_exception() throws Exception {
		doThrow(FileSystemSegmentException.class).when(segment1).close();

		storage.close();

		verify(segment1).tryLock();
		verify(segment1).close();
		verify(segment1).unlock();
	}


	private List<FileStorageSegment> getSegmentsList() {
		List<FileStorageSegment> list = new LinkedList<>();
		list.add(segment1);
		list.add(segment2);
		list.add(segment3);
		return list;
	}


}
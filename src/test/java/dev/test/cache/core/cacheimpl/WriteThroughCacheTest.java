package dev.test.cache.core.cacheimpl;

import dev.test.cache.MockConfiguration;
import dev.test.cache.config.CachingStrategy;
import dev.test.cache.core.exceptions.CacheReadException;
import dev.test.cache.core.exceptions.CacheSystemException;
import dev.test.cache.core.exceptions.CacheWriteException;
import dev.test.cache.core.storage.FileSystemStorage;
import dev.test.cache.core.storage.RamStorage;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemStorageException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;

import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.BDDMockito.then;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.MockitoAnnotations.initMocks;


public class WriteThroughCacheTest {

	private WriteThroughCache<String, String> cache;

	private KeyValuePair<String, String> pair;

	@Mock
	private FileSystemStorage<String, String> fileSystemStorage;

	@Mock
	private RamStorage<String, String> ramStorage;



	@Before
	public void setUp() throws Exception {
		initMocks(this);
		pair = new KeyValuePair<>("key", "value");
		cache = new WriteThroughCache<>(MockConfiguration.getCacheConfiguration(CachingStrategy.WRITE_THROUGH), "cache_id");
		cache.close(); // to close automatically initialized storages
		cache.fileSystemStorage = fileSystemStorage;
		cache.ramStorage = ramStorage;
	}


	@After
	public void tearDown() throws Exception {

	}


	// Put
	@Test
	public void not_evicted_put() throws Exception {
		given(ramStorage.save(pair)).willReturn(KeyValuePair.empty());
		given(fileSystemStorage.save(pair)).willReturn(false);

		cache.doPut(pair);

		verify(ramStorage).save(pair);
		verify(fileSystemStorage).save(pair);
	}


	@Test(expected = CacheWriteException.class)
	public void put_with_exception() throws Exception {
		given(ramStorage.save(pair)).willReturn(KeyValuePair.empty());
		given(fileSystemStorage.save(pair)).willThrow(FileSystemStorageException.class);

		cache.put(pair.getKey(), pair.getValue());

		verify(ramStorage).save(pair);
		verify(fileSystemStorage).save(pair);
	}



	// replace
	@Test
	public void when_replace_then_replaced_in_both_cache_levels() throws Exception {
		given(ramStorage.replace(pair)).willReturn(true);
		given(fileSystemStorage.replace(pair)).willReturn(true);

		final boolean replaced = cache.replace(pair.getKey(), pair.getValue());

		assertThat(replaced).isTrue();
		verify(ramStorage).replace(pair);
		verify(fileSystemStorage).replace(pair);
	}


	@Test
	public void when_replace_non_existent_then_false() throws Exception {
		given(ramStorage.replace(pair)).willReturn(false);
		given(fileSystemStorage.replace(pair)).willReturn(false);

		final boolean replaced = cache.replace(pair.getKey(), pair.getValue());

		assertThat(replaced).isFalse();
		verify(ramStorage).replace(pair);
		verify(fileSystemStorage).replace(pair);
	}


	// remove
	@Test
	public void when_remove_then_removed_from_both_cache_levels() throws Exception {
		given(ramStorage.remove(pair.getKey())).willReturn(KeyValuePair.empty());
		given(fileSystemStorage.remove(pair.getSerializedKey())).willReturn(true);

		final boolean removed = cache.remove(pair.getKey());

		assertThat(removed).isTrue();
		verify(ramStorage).remove(pair.getKey());
		verify(fileSystemStorage).remove(pair.getSerializedKey());
	}


	@Test
	public void when_removed_non_existent_then_false_returned() throws Exception {
		given(ramStorage.remove(pair.getKey())).willReturn(KeyValuePair.empty());
		given(fileSystemStorage.remove(pair.getSerializedKey())).willReturn(false);

		final boolean removed = cache.remove(pair.getKey());

		assertThat(removed).isFalse();
		verify(ramStorage).remove(pair.getKey());
		verify(fileSystemStorage).remove(pair.getSerializedKey());
	}


	@Test(expected = CacheWriteException.class)
	public void when_storage_fails_then_exception_thrown() throws Exception {
		given(ramStorage.remove(pair.getKey())).willReturn(KeyValuePair.empty());
		given(fileSystemStorage.remove(pair.getSerializedKey())).willThrow(FileSystemStorageException.class);

		cache.remove(pair.getKey());

		verify(ramStorage).remove(pair.getKey());
		verify(fileSystemStorage).remove(pair.getSerializedKey());
	}


	// get
	@Test
	public void when_get_from_ram_successful_then_no_interactions_with_fs() throws Exception {
		given(ramStorage.get(pair.getKey())).willReturn(pair.getSerializedValue());

		final String value = cache.get(pair.getKey());

		assertThat(value).isEqualTo(pair.getValue());
		verify(ramStorage).get(pair.getKey());
		verifyZeroInteractions(fileSystemStorage);
	}


	@Test
	public void when_miss_from_ram_then_fs_checked() throws Exception {
		given(ramStorage.get(pair.getKey())).willReturn(null);
		given(fileSystemStorage.get(pair.getSerializedKey())).willReturn(pair.getSerializedValue());

		final String value = cache.get(pair.getKey());

		assertThat(value).isEqualTo(pair.getValue());
		verify(ramStorage).get(pair.getKey());
		verify(fileSystemStorage).get(pair.getSerializedKey());
	}


	@Test
	public void when_miss_from_ram_and_fs_then_null() throws Exception {
		given(ramStorage.get(pair.getKey())).willReturn(null);
		given(fileSystemStorage.get(pair.getSerializedKey())).willReturn(null);

		final String value = cache.get(pair.getKey());

		assertThat(value).isNull();
		verify(ramStorage).get(pair.getKey());
		verify(fileSystemStorage).get(pair.getSerializedKey());
	}


	@Test(expected = CacheReadException.class)
	public void when_get_storage_fails_then_exception_thrown() throws Exception {
		given(fileSystemStorage.get(pair.getSerializedKey())).willThrow(FileSystemStorageException.class);
		cache.get(pair.getKey());

		verify(ramStorage).get(pair.getKey());
		verify(fileSystemStorage).get(pair.getSerializedKey());
	}


	@Test
	public void when_contains_is_true_from_ram_then_no_interactions_with_fs() throws Exception {
		given(ramStorage.contains(pair.getKey())).willReturn(true);

		final boolean contains = cache.contains(pair.getKey());

		assertThat(contains).isTrue();
		verify(ramStorage).contains(pair.getKey());
		verifyZeroInteractions(fileSystemStorage);
	}


	@Test
	public void when_contains_is_false_from_ram_then_fs_checked() throws Exception {
		given(ramStorage.contains(pair.getKey())).willReturn(false);
		given(fileSystemStorage.contains(pair.getSerializedKey())).willReturn(true);

		final boolean contains = cache.contains(pair.getKey());

		assertThat(contains).isTrue();
		verify(ramStorage).contains(pair.getKey());
		verify(fileSystemStorage).contains(pair.getSerializedKey());
	}


	@Test
	public void when_contains_false_in_both_levels_then_false() throws Exception {
		given(ramStorage.contains(pair.getKey())).willReturn(false);
		given(fileSystemStorage.contains(pair.getSerializedKey())).willReturn(false);

		final boolean contains = cache.contains(pair.getKey());

		assertThat(contains).isFalse();
		verify(ramStorage).contains(pair.getKey());
		verify(fileSystemStorage).contains(pair.getSerializedKey());
	}


	@Test(expected = CacheReadException.class)
	public void when_contains_storage_fails_then_exception() throws Exception {
		given(fileSystemStorage.contains(pair.getSerializedKey())).willThrow(FileSystemStorageException.class);

		cache.contains(pair.getKey());

		verify(ramStorage).contains(pair.getKey());
		verify(fileSystemStorage).contains(pair.getSerializedKey());
	}


	@Test
	public void when_clear_then_both_storages_cleared() throws Exception {
		cache.clear();

		verify(ramStorage).clear();
		verify(fileSystemStorage).clear();
	}


	@Test(expected = CacheSystemException.class)
	public void when_clear_storage_fails_then_exception() throws Exception {
		doThrow(FileSystemStorageException.class).when(fileSystemStorage).clear();

		cache.clear();

		verify(ramStorage).clear();
		verify(fileSystemStorage).clear();
	}


	@Test
	public void when_close_then_both_storages_closed() throws Exception {
		cache.close();

		verify(ramStorage).close();
		verify(fileSystemStorage).close();
	}


	@Test(expected = CacheSystemException.class)
	public void when_close_storage_fails_then_exception() throws Exception {
		doThrow(FileSystemStorageException.class).when(fileSystemStorage).close();

		cache.close();

		verify(ramStorage).close();
		verify(fileSystemStorage).close();
	}



}




































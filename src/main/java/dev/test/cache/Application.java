package dev.test.cache;

import dev.test.cache.config.CachingStrategy;
import dev.test.cache.config.EvictionPolicy;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.Cache;
import dev.test.cache.core.manager.CacheManager;
import dev.test.cache.core.manager.CacheManagerBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;


public class Application {

	private static final Logger LOG = LoggerFactory.getLogger(Application.class);

	private static final String CACHE_NAME = "test-cache";

	private static final AtomicInteger counter = new AtomicInteger(0);

	private static volatile boolean flag = false;

	private static final Cache<Integer, String> cache = getCache();


	public static void main(String[] args) throws Exception {
		logDemo();
		startProducer();
		startConsumer();
		Runtime.getRuntime().addShutdownHook(new Thread(cache::close));
	}


	private static void logDemo() {
		LOG.info("This is just a little demo");
		sleep(1000);
		LOG.info("Currently using write-back implementation of cache. Eviction from RAM will start at 10");
		sleep(3000);
	}


	private static void startProducer() {
		new Thread(() -> {

			while (true) {

				if (!flag) {
					final int key = counter.incrementAndGet();
					LOG.info("Putting to cache, key: {}", key);
					cache.put(key, UUID.randomUUID().toString());
					sleep(1000);
					flag = true;
				}

			}

		}).start();
	}


	private static void startConsumer() {
		new Thread(() -> {

			while (true) {

				if (flag) {
					final int key = counter.get();
					LOG.info("Reading from cache, key: {}", key);
					cache.get(key);
					sleep(1000);
					flag = false;
				}
			}

		}).start();
	}


	private static void sleep(final long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	private static Cache<Integer, String> getCache() {
		final CacheManager cacheManager = CacheManagerBuilder.newInstance()
				.withMemoryConfiguration(new RamConfiguration(
						10, EvictionPolicy.LRU
				))
				.withFileSystemConfiguration(new FileSystemConfiguration(
						"cache_test",
						1, SizeUnit.MB,
						false, EvictionPolicy.LRU,
						4
				))
				.withCachingStrategy(CachingStrategy.WRITE_BACK)
				.build();

		return cacheManager.makeCache(CACHE_NAME, Integer.class, String.class);
	}


}

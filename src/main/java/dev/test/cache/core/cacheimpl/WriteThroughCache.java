package dev.test.cache.core.cacheimpl;

import dev.test.cache.config.CacheConfiguration;
import dev.test.cache.core.serializer.Serializer;
import dev.test.cache.core.storage.holders.KeyValuePair;


// Writes key-value in both levels of cache
public class WriteThroughCache<K, V> extends AbstractCache<K, V> {


	public WriteThroughCache(final CacheConfiguration cacheConfiguration, final String cacheId) {
		super(cacheConfiguration, cacheId);
	}


	@Override
	protected void doPut(final KeyValuePair<K, V> keyValuePair) {
		final KeyValuePair<K, V> evictedRam = ramStorage.save(keyValuePair);
		final boolean evictedFileStorageSegment = fileSystemStorage.save(keyValuePair);

		if (evictedRam.isPresent()) {
			LOG.debug("Evicted from 1L cache (RAM storage)");
		}

		if (evictedFileStorageSegment) {
			LOG.debug("Evicted segment from 2L cache (file-system storage)");
		}

	}


	@Override
	protected boolean doReplace(final KeyValuePair<K, V> keyValuePair) {
		final boolean replacedRam = ramStorage.replace(keyValuePair);
		final boolean replacedFileStorageSegment = fileSystemStorage.replace(keyValuePair);

		if (replacedRam || replacedFileStorageSegment) {
			LOG.debug("Value was replaced in cache");
			return true;
		}

		LOG.debug("Value was replaced in cache, key wasn't found");

		return false;
	}


	@Override
	protected boolean doRemove(K key) {
		final KeyValuePair<K, V> removedRam = ramStorage.remove(key);
		final boolean removedFileStorage = fileSystemStorage.remove(Serializer.serialize(key));

		if (removedRam.isPresent() || removedFileStorage) {
			LOG.debug("Value was removed from cache");
			return true;
		}

		LOG.debug("Value wasn't removed fom cache, key wasn't found");

		return false;
	}


	@Override
	protected byte[] doGet(K key) {
		byte[] value = ramStorage.get(key);

		if (value == null) {
			LOG.debug("Miss from 1L RAM cache. Trying to get from 2L");
			value = fileSystemStorage.get(Serializer.serialize(key));
		}

		if (value == null) {
			LOG.debug("Miss from 2L cache");
		}

		return value;
	}




}

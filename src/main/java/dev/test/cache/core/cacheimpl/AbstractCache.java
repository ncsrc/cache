package dev.test.cache.core.cacheimpl;

import dev.test.cache.config.CacheConfiguration;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.Cache;
import dev.test.cache.core.exceptions.CacheReadException;
import dev.test.cache.core.exceptions.CacheShutdownException;
import dev.test.cache.core.exceptions.CacheSystemException;
import dev.test.cache.core.exceptions.CacheWriteException;
import dev.test.cache.core.serializer.Serializer;
import dev.test.cache.core.storage.FileSystemStorage;
import dev.test.cache.core.storage.RamStorage;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.FifoFileSystemStorage;
import dev.test.cache.core.storage.impl.filesystem.LruFileSystemStorage;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemStorageException;
import dev.test.cache.core.storage.impl.ram.FifoRamStorage;
import dev.test.cache.core.storage.impl.ram.LruRamStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public abstract class AbstractCache<K, V> implements Cache<K, V> {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractCache.class);

	protected final CacheConfiguration cacheConfiguration;

	protected RamStorage<K, V> ramStorage;

	protected FileSystemStorage<K, V> fileSystemStorage;


	public AbstractCache(final CacheConfiguration cacheConfiguration, final String cacheId) {
		this.cacheConfiguration = cacheConfiguration;
		this.ramStorage = StorageFactory.newRamStorage(cacheConfiguration.getRamConfiguration());
		this.fileSystemStorage = StorageFactory.newFileSystemStorage(cacheConfiguration.getFileSystemConfiguration(), cacheId);
	}


	@Override
	public void put(K key, V value) throws CacheWriteException {
		LOG.debug("Putting in cache. Key: {}; Value: {}", key, value);

		final KeyValuePair<K, V> keyValuePair = new KeyValuePair<>(key, value);

		try {
			doPut(keyValuePair);	// key, Serializer.serialize(value)
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to put key: {} and value: {} in cache", key, value);
			throw new CacheWriteException("Failed to put key-value in cache", e);
		}

	}


	protected abstract void doPut(final KeyValuePair<K, V> keyValuePair);


	@Override
	public boolean replace(K key, V value) throws CacheWriteException {
		LOG.debug("Replacing in cache. Key: {}; Value: {}", key, value);
		final KeyValuePair<K, V> keyValuePair = new KeyValuePair<>(key, value);

		try {
			return doReplace(keyValuePair);
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to replace value: {} with key: {} in cache", value, key);
			throw new CacheWriteException("Failed to replace value", e);
		}

	}


	protected abstract boolean doReplace(final KeyValuePair<K, V> keyValuePair);


	@Override
	public boolean remove(K key) throws CacheWriteException {
		LOG.debug("Removing value in cache by key: {}", key);

		try {
			return doRemove(key);
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to remove value by key: {}", key);
			throw new CacheWriteException("Failed to remove value", e);
		}

	}


	protected abstract boolean doRemove(K key);


	@Override
	@SuppressWarnings("unchecked")
	public V get(K key) throws CacheReadException {
		LOG.debug("Starting to get value from cache by key: {}", key);

		try {
			final byte[] serializedValue = doGet(key);
			return (serializedValue == null) ? null : (V) Serializer.deserializeValue(serializedValue);
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to read value from cache", e);
			throw new CacheReadException("Failed to read value from cache", e);
		}

	}


	protected abstract byte[] doGet(K key);


	@Override
	public boolean contains(K key) throws CacheReadException {
		LOG.debug("Checking if key {} exists in cache", key);

		try {
			return ramStorage.contains(key) || fileSystemStorage.contains(Serializer.serialize(key));
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to perform contains check in cache", e);
			throw new CacheReadException("Failed to perform contains check in cache", e);
		}

	}


	@Override
	public void clear() throws CacheSystemException {
		LOG.debug("Clearing cache");

		try {
			ramStorage.clear();
			fileSystemStorage.clear();
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to clear cache", e);
			throw new CacheSystemException("Failed to clear cache", e);
		}

	}


	@Override
	public void close() throws CacheShutdownException {
		LOG.info("Closing cache");

		try {
			ramStorage.close();
			fileSystemStorage.close();
		} catch (FileSystemStorageException e) {
			LOG.warn("Failed to close cache", e);
			throw new CacheReadException("Failed to close cache", e);
		}

	}


	private static final class StorageFactory {

		static <K, V> RamStorage<K, V> newRamStorage(final RamConfiguration ramConfiguration) {
			switch(ramConfiguration.getEvictionPolicy()) {
				case LRU:
					return new LruRamStorage<>(ramConfiguration);

				case FIFO:
					return new FifoRamStorage<>(ramConfiguration);

				default: return new LruRamStorage<>(ramConfiguration);
			}
		}


		static <K, V> FileSystemStorage<K, V> newFileSystemStorage(final FileSystemConfiguration fileSystemConfiguration, final String cacheId) {
			switch(fileSystemConfiguration.getEvictionPolicy()) {
				case LRU:
					return new LruFileSystemStorage<>(fileSystemConfiguration, cacheId);

				case FIFO:
					return new FifoFileSystemStorage<>(fileSystemConfiguration, cacheId);

				default: return new FifoFileSystemStorage<>(fileSystemConfiguration, cacheId);
			}
		}


	}

}

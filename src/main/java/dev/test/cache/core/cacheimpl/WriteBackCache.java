package dev.test.cache.core.cacheimpl;

import dev.test.cache.config.CacheConfiguration;
import dev.test.cache.core.serializer.Serializer;
import dev.test.cache.core.storage.holders.KeyValuePair;


// Fills RAM storage and when it's full, evicts to FS storage
public class WriteBackCache<K, V> extends AbstractCache<K, V> {


	public WriteBackCache(final CacheConfiguration cacheConfiguration, final String cacheId) {
		super(cacheConfiguration, cacheId);
	}


	@Override
	protected void doPut(final KeyValuePair<K, V> keyValuePair) {
		final KeyValuePair<K, V> evicted = ramStorage.save(keyValuePair);

		if (evicted.isPresent()) {
			LOG.debug("Got evicted value from RAM cache. Saving it to file system");
			fileSystemStorage.save(evicted);
		}
	}


	@Override
	protected boolean doReplace(final KeyValuePair<K, V> keyValuePair) {
		boolean replaced = ramStorage.replace(keyValuePair);

		if (!replaced) {
			LOG.debug("Could not find specified key-value in 1L cache. Replacing in 2L.");
			replaced = fileSystemStorage.replace(keyValuePair);
		}

		return replaced;
	}


	@Override
	protected boolean doRemove(K key) {
		final KeyValuePair<K, V> removed = ramStorage.remove(key);

		if (removed.isPresent()) {
			LOG.debug("Found and removed key {} in RAM storage", key);
			return true;
		}

		if (fileSystemStorage.remove(Serializer.serialize(key))) {
			LOG.debug("Found and removed key {} in file system storage", key);
			return true;
		}

		return false;
	}


	@Override
	protected byte[] doGet(K key) {
		byte[] value = ramStorage.get(key);

		if (value == null) {
			LOG.debug("Miss from 1L RAM cache. Trying to get from 2L");
			value = fileSystemStorage.get(Serializer.serialize(key));
		}

		if (value == null) {
			LOG.debug("Miss from 2L cache");
		}

		return value;
	}


}

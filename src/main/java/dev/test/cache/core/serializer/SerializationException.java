package dev.test.cache.core.serializer;


public class SerializationException extends RuntimeException {

	public SerializationException() {
		super();
	}

	public SerializationException(String message) {
		super(message);
	}

	public SerializationException(String message, Throwable cause) {
		super(message, cause);
	}

}

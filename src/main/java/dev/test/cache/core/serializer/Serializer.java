package dev.test.cache.core.serializer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;


// Utility class for (de)serialization
public class Serializer {

	private static final Logger LOG = LoggerFactory.getLogger(Serializer.class);


	public static <T> byte[] serialize(T value) throws SerializationException {
		LOG.debug("Starting to serialize: {}", value);

		try (final ByteArrayOutputStream byteOutputStream = new ByteArrayOutputStream()) {
			final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteOutputStream);
			objectOutputStream.writeObject(value);
			objectOutputStream.close();

			return byteOutputStream.toByteArray();
		} catch (IOException e) {
			LOG.warn("Error during value serialization has occurred", e);
			throw new SerializationException("Error during value serialization has occurred", e);
		}

	}


	@SuppressWarnings("unchecked")
	public static <T> T deserializeValue(final byte[] bytes) throws SerializationException {

		try (final ObjectInputStream objectInputStream = new ObjectInputStream(new ByteArrayInputStream(bytes))) {
			return (T) objectInputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			LOG.warn("Error during value deserialization has occurred", e);
			throw new SerializationException("Error during value deserialization has occurred", e);
		}

	}


}

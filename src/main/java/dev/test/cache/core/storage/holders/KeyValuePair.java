package dev.test.cache.core.storage.holders;

import dev.test.cache.core.serializer.Serializer;

import java.util.Arrays;


public class KeyValuePair<K, V> {

	private K key;
	private V value;
	private byte[] serializedKey;
	private byte[] serializedValue;


	public static <K, V> KeyValuePair<K, V> empty() {
		return new KeyValuePair<>();
	}


	private KeyValuePair() {
//		this.key = null;
//		this.serializedValue = null;
	}


	public KeyValuePair(K key, byte[] serializedValue) {
		this.key = key;
		this.serializedKey = Serializer.serialize(key);
		this.serializedValue = serializedValue;
	}


	public KeyValuePair(byte[] serializedKey, byte[] serializedValue) {
		this.serializedKey = serializedKey;
		this.serializedValue = serializedValue;
	}


	public KeyValuePair(K key, V value) {
		this.key = key;
		this.value = value;
		this.serializedKey = Serializer.serialize(key);
		this.serializedValue = Serializer.serialize(value);
	}


	public K getKey() {
		return key;
	}


	public V getValue() {
		return value;
	}


	public byte[] getSerializedKey() {
		return serializedKey;
	}


	public byte[] getSerializedValue() {
		return serializedValue;
	}


	public void setSerializedValue(byte[] serializedValue) {
		this.serializedValue = serializedValue;
	}


	public void setKey(K key) {
		this.key = key;
	}


	public boolean isPresent() {
		return serializedKey != null && serializedValue != null;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;

		KeyValuePair<?, ?> that = (KeyValuePair<?, ?>) o;

		if (key != null ? !key.equals(that.key) : that.key != null) return false;
		if (value != null ? !value.equals(that.value) : that.value != null) return false;
		if (!Arrays.equals(serializedKey, that.serializedKey)) return false;
		return Arrays.equals(serializedValue, that.serializedValue);
	}


	@Override
	public int hashCode() {
		int result = key != null ? key.hashCode() : 0;
		result = 31 * result + (value != null ? value.hashCode() : 0);
		result = 31 * result + Arrays.hashCode(serializedKey);
		result = 31 * result + Arrays.hashCode(serializedValue);
		return result;
	}


	@Override
	public String toString() {
		return "KeyValuePair{" +
				"key=" + key +
				", value=" + value +
				", serializedKey=" + Arrays.toString(serializedKey) +
				", serializedValue=" + Arrays.toString(serializedValue) +
				'}';
	}
}

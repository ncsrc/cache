package dev.test.cache.core.storage;

import dev.test.cache.core.storage.holders.KeyValuePair;


public interface RamStorage<K, V> extends AutoCloseable {

	// Returns evicted key-value pair
	KeyValuePair<K, V> save(KeyValuePair<K, V> keyValuePair);

	// Returns true if replaced successfully
	boolean replace(KeyValuePair<K, V> keyValuePair);

	byte[] get(K key);

	// Returns removed key-value pair; needed for eviction
	KeyValuePair<K, V> remove(K key);

	void clear();

	boolean contains(K key);

	@Override
	void close();


}

package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.core.storage.FileSystemStorage;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemSegmentException;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemStorageException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.nio.file.*;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.stream.Collectors;


abstract class AbstractFileSystemStorage<K, V> implements FileSystemStorage<K, V> {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractFileSystemStorage.class);

	protected final Path rootDirectory;

	protected final FileSystemConfiguration configuration;

	protected List<FileStorageSegment> segments;

	protected final Queue<FileStorageSegment> fullSegments = new ConcurrentLinkedQueue<>();


	AbstractFileSystemStorage(final FileSystemConfiguration configuration, final String cacheId) throws FileSystemStorageException {
		this.segments = Collections.synchronizedList(new LinkedList<FileStorageSegment>());
		this.configuration = configuration;
		this.rootDirectory = Paths.get(getFullLocation(configuration, cacheId));

		try {

			if (configuration.isPersistent() && storageExists()) {
				initExistingStorage();
			} else {
				prepareNewStorage();
				initNewStorage();
			}

		} catch (IOException e) {
			LOG.warn("Error during initialization of file system storage has occurred", e);
			throw new FileSystemStorageException("Error during initialization of file system storage has occurred", e);
		}

	}


	/*
		check for eviction - if all segments are full, evict (remake last); check lock
		eviction: first fulled segment
		tryLock until success
		write
		unlock
	*/
	@Override
	public boolean save(KeyValuePair<K, V> keyValuePair) throws FileSystemStorageException {
		LOG.debug("Saving key-value pair to file system storage");
		boolean isEvicted = false;
		FileStorageSegment segment = null;

		try {
			// critical section
			synchronized (this) {
				if (fullSegments.size() == configuration.getSegmentsAmount()) {
					LOG.debug("All segments are full. Starting to evict.");
					segment = performEviction();
					isEvicted = true;
				} else {
					segment = getNotFullSegment();
				}

			}

			segment.write(keyValuePair.getSerializedKey(), keyValuePair.getSerializedValue());

			if (segment.isFull()) {
				fullSegments.add(segment);
			}

		} catch (FileSystemSegmentException e) {
			LOG.warn("Failed to save key-value pair in file system storage", e);
			throw new FileSystemStorageException("Failed to save key-value pair in file system storage", e);

		} finally {
			if (segment != null) {
				segment.unlock();
			}
		}

		return isEvicted;
	}


	// Evicts old segment with new one, locks it
	protected abstract FileStorageSegment performEviction();


	@Override
	public boolean replace(KeyValuePair<K, V> keyValuePair) throws FileSystemStorageException {
		LOG.debug("Replacing key-value pair in file system storage");
		final Set<FileStorageSegment> checkedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(checkedSegments)) != null) {

			try {
				final boolean replaced = segment.replace(keyValuePair.getSerializedKey(), keyValuePair.getSerializedValue());
				if (!replaced) {
					checkedSegments.add(segment);
				} else {
					afterReplace(segment);
					return true;
				}
			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to replace key-value pair in file system storage", e);
				throw new FileSystemStorageException("Failed to replace key-value pair in file system storage", e);

			} finally {
				segment.unlock();
			}

		}

		return false;
	}

	protected abstract void afterReplace(FileStorageSegment segment);

	@Override
	public byte[] get(byte[] key) throws FileSystemStorageException {
		LOG.debug("Getting value from file system storage");
		final Set<FileStorageSegment> checkedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(checkedSegments)) != null) {

			try {
				final byte[] value = segment.get(key);

				if (value == null) {
					checkedSegments.add(segment);
				} else {
					afterGet(segment);
					return value;
				}

			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to get value from file system storage", e);
				throw new FileSystemStorageException("Failed to get value from file system storage", e);

			} finally {
				segment.unlock();
			}

		}

		// if miss
		return null;
	}

	protected abstract void afterGet(FileStorageSegment segment);

	@Override
	public boolean remove(byte[] key) throws FileSystemStorageException {
		LOG.debug("Removing key-value pair from file system storage");
		final Set<FileStorageSegment> checkedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(checkedSegments)) != null) {

			try {
				final boolean removed = segment.remove(key);

				if (!removed) {
					checkedSegments.add(segment);
				} else {
					afterRemove(segment);
					return true;
				}
			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to remove value from file system storage", e);
				throw new FileSystemStorageException("Failed to remove value from file system storage", e);

			} finally {
				segment.unlock();
			}

		}

		return false;
	}


	protected abstract void afterRemove(FileStorageSegment segment);


	@Override
	public boolean contains(byte[] key) throws FileSystemStorageException {
		final Set<FileStorageSegment> checkedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(checkedSegments)) != null) {

			try {
				final boolean found = segment.contains(key);

				if (!found) {
					checkedSegments.add(segment);
				} else {
					afterContains(segment);
					return true;
				}

			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to perform contains check in file system storage", e);
				throw new FileSystemStorageException("Failed to perform contains check in file system storage", e);

			} finally {
				segment.unlock();
			}

		}

		return false;
	}


	protected abstract void afterContains(FileStorageSegment segment);


	@Override
	public void clear() throws FileSystemStorageException {
		LOG.debug("Clearing file system storage");
		final Set<FileStorageSegment> clearedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(clearedSegments)) != null) {

			try {
				segment.clear();
				clearedSegments.add(segment);

			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to clear file system storage", e);
				throw new FileSystemStorageException("Failed to clear file system storage", e);

			} finally {
				segment.unlock();
			}

		}

	}


	@Override
	public void close() throws FileSystemStorageException {
		LOG.info("Closing file system storage");
		final Set<FileStorageSegment> closedSegments = new HashSet<>();

		FileStorageSegment segment;
		while ((segment = getSegment(closedSegments)) != null) {

			try {
				segment.close();
				closedSegments.add(segment);

			} catch (FileSystemSegmentException e) {
				LOG.warn("Failed to close file system storage", e);
				throw new FileSystemStorageException("Failed to close file system storage", e);

			} finally {
				segment.unlock();
			}

		}

	}



	private void initExistingStorage() throws IOException {
		for (File file : rootDirectory.toFile().listFiles()) {
			segments.add(FileStorageSegment.from(file, configuration));
		}
	}

	private void initNewStorage() throws IOException {
		for (int i = 0; i < configuration.getSegmentsAmount(); i++) {
			segments.add(new FileStorageSegment(rootDirectory, configuration));
		}
	}

	// Removes possibly existing dirs with its content and makes a new ones
	private void prepareNewStorage() throws IOException {
		if (rootDirExists()) {
			removeRootDirContent();
		}

		Files.createDirectories(rootDirectory);
	}

	private void removeRootDirContent() throws IOException {
		Files.walkFileTree(rootDirectory, new SimpleFileVisitor<Path>() {

			@Override
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				Files.delete(file);
				return FileVisitResult.CONTINUE;
			}

			@Override
			public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
				Files.delete(dir);
				return FileVisitResult.CONTINUE;
			}

		});
	}


	private String getFullLocation(FileSystemConfiguration configuration, String cacheId) {
		return configuration.getLocation() + "/cache_" + cacheId;
	}

	private boolean storageExists() {
		return rootDirExists() && (rootDirectory.toFile().listFiles().length > 0);
	}

	private boolean rootDirExists() {
		return rootDirectory.toFile().exists();
	}



	private FileStorageSegment getNotFullSegment() {
		final List<FileStorageSegment> notFull = segments.stream()
				.filter(segment -> !fullSegments.contains(segment))
				.collect(Collectors.toList());

		return getSegment(notFull);
	}


	// Gets segment, excluding those in set, may return null
	private FileStorageSegment getSegment(final Set<FileStorageSegment> excluded) {
		final LinkedList<FileStorageSegment> copied = new LinkedList<>(segments);
		copied.removeAll(excluded);
		return getSegment(copied);
	}


	// possible starvation in highly concurrent environment ?
	// Picks from specified list
	private FileStorageSegment getSegment(final List<FileStorageSegment> segments) {
		LOG.debug("Trying to get and lock segment from: {}", segments);
		FileStorageSegment lockedSegment = null;

		if (segments.isEmpty()) {
			LOG.debug("There are no segments to choose from");
			return null;
		}

		boolean lockIsAcquired = false;
		while (!lockIsAcquired) {
			for (FileStorageSegment segment : segments) {
				if (segment.tryLock()) {
					LOG.debug("Successfully locked and acquired segment: {}", segment);
					lockedSegment = segment;
					lockIsAcquired = true;
					break;
				}
			}
		}

		return lockedSegment;
	}



}

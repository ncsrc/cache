package dev.test.cache.core.storage.impl.filesystem.exceptions;


public class FileSystemSegmentException extends RuntimeException {

	public FileSystemSegmentException() {
		super();
	}

	public FileSystemSegmentException(String message) {
		super(message);
	}

	public FileSystemSegmentException(String message, Throwable cause) {
		super(message, cause);
	}

}

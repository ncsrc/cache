package dev.test.cache.core.storage.holders;

import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;


public class ValueHolder {

	private byte[] serializedValue;
	private AtomicInteger hitCounter;
	private AtomicLong creationTime;


	public ValueHolder(byte[] value) {
		this.serializedValue = value;
		this.hitCounter = new AtomicInteger(0);
		this.creationTime = new AtomicLong(System.currentTimeMillis());
	}



	public byte[] getSerializedValue() {
		return serializedValue;
	}


	public int getHitCounter() {
		return hitCounter.get();
	}


	public int incrementHitCounter() {
		return hitCounter.incrementAndGet();
	}


	public long getCreationTime() {
		return creationTime.get();
	}


}

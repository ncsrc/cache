package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemSegmentException;


public class FifoFileSystemStorage<K, V> extends AbstractFileSystemStorage<K, V> {


	public FifoFileSystemStorage(final FileSystemConfiguration configuration, final String cacheId) {
		super(configuration, cacheId);
	}


	@Override
	protected void afterReplace(FileStorageSegment segment) {
		LOG.debug("Successfully replaced value");
		/* NOP */
	}


	@Override
	protected void afterGet(FileStorageSegment segment) {
		LOG.debug("Successfully got value");
		/* NOP */
	}


	@Override
	protected void afterRemove(FileStorageSegment segment) {
		LOG.debug("Successfully removed value");
		/* NOP */
	}


	@Override
	protected void afterContains(FileStorageSegment segment) {
		LOG.debug("Successfully found key");
		/* NOP */
	}


	@Override
	protected FileStorageSegment performEviction() {
		evict();
		return makeNewSegment();
	}


	private FileStorageSegment makeNewSegment() {
		final FileStorageSegment newSegment = new FileStorageSegment(rootDirectory, configuration);
		newSegment.tryLock();
		segments.add(0, newSegment);
		return newSegment;
	}


	private void evict() {
		final FileStorageSegment fullSegment = fullSegments.poll();
		segments.remove(fullSegment);

		try {
			fullSegment.lock();
			fullSegment.close();
		} catch (FileSystemSegmentException e) {
			LOG.warn("Failed to close segment while performing eviction", e);
		} finally {
			fullSegment.unlock();
		}

	}


}

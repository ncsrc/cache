package dev.test.cache.core.storage.impl.filesystem.exceptions;


public class FileSystemStorageException extends RuntimeException {


	public FileSystemStorageException() {
		super();
	}

	public FileSystemStorageException(String message) {
		super(message);
	}

	public FileSystemStorageException(String message, Throwable cause) {
		super(message, cause);
	}

}

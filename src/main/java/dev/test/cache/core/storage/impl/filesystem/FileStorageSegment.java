package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemSegmentException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;


public class FileStorageSegment implements AutoCloseable {

	private static final Logger LOG = LoggerFactory.getLogger(FileStorageSegment.class);

	private final Lock lock = new ReentrantLock();

	private BufferedOutputStream outputStream;

	private final String fileLocation;

	private final boolean isPersistent;

	private long maxSize;	// in bytes

	private AtomicLong currentSize = new AtomicLong(0);



	public FileStorageSegment(final File file, final FileSystemConfiguration configuration) throws FileSystemSegmentException {
		calculateMaxSize(configuration);
		this.fileLocation = file.getAbsolutePath();
		this.isPersistent = configuration.isPersistent();

		try {
			replaceOutputStream(true);
		} catch (IOException e) {
			LOG.warn("Failed to make file storage segment", e);
			throw new FileSystemSegmentException("Failed to make file storage segment", e);
		}

	}

	public FileStorageSegment(final Path rootDirectory, final FileSystemConfiguration configuration) throws FileSystemSegmentException {
		calculateMaxSize(configuration);
		this.fileLocation = rootDirectory + File.separator + UUID.randomUUID();
		this.isPersistent = configuration.isPersistent();

		try {
			replaceOutputStream(false);
		} catch (IOException e) {
			LOG.warn("Failed to make file storage segment", e);
			throw new FileSystemSegmentException("Failed to make file storage segment", e);
		}
	}

	public static FileStorageSegment from(final File file, final FileSystemConfiguration configuration) throws FileSystemSegmentException {
		return new FileStorageSegment(file, configuration);
	}


	public boolean tryLock() {
		LOG.debug("Trying to lock segment");
		final boolean locked = lock.tryLock();

		if (locked) {
			LOG.debug("Successfully locked segment");
		} else {
			LOG.debug("Segment is already locked");
		}

		return locked;
	}


	public void lock() {
		LOG.debug("Locking segment");
		lock.lock();
	}


	public void unlock() {
		LOG.debug("Unlocking segment");
		lock.unlock();
	}


	public boolean isFull() {
		return currentSize.get() >= maxSize;
	}


	public void write(final byte[] key, final byte[] value) throws FileSystemSegmentException {
		LOG.debug("Writing to file storage segment");
		try {
			write(key);
			write(value);
			currentSize.getAndAdd(key.length + value.length);
		} catch (IOException e) {
			LOG.warn("Failed to write to file storage segment");
			throw new FileSystemSegmentException("Failed to write to file storage segment", e);
		}
	}


	public byte[] get(final byte[] key) throws FileSystemSegmentException {
		LOG.debug("Reading from file storage segment");

		try (final SegmentIterator segmentIterator = SegmentIterator.newInstance(fileLocation)) {

			while (segmentIterator.hasNext()) {
				final KeyValuePair pair = segmentIterator.next();

				if (pair.isPresent()) {
					if (keysAreEqual(key, pair)) {
						LOG.debug("Successfully found key in file storage segment");
						return pair.getSerializedValue();
					}
				}

			}

		} catch (IOException e) {
			LOG.warn("Failed to read from file storage segment");
			throw new FileSystemSegmentException("Failed to read from file storage segment", e);
		}

		// If miss
		LOG.debug("Not found specified key in file storage segment");
		return null;
	}


	// returns true if found and replaced key-value
	// yes, it is highly inefficient operation
	public boolean replace(final byte[] key, final byte[] value) throws FileSystemSegmentException {
		LOG.debug("Starting to replace");

		if (contains(key)) {
			currentSize.set(0);

			try (final SegmentIterator segmentIterator = SegmentIterator.newInstance(fileLocation)) {
				replaceOutputStream(false);

				while (segmentIterator.hasNext()) {
					final KeyValuePair pair = segmentIterator.next();

					if (keysAreEqual(key, pair)) {
						pair.setSerializedValue(value);
					}

					write(pair.getSerializedKey(), pair.getSerializedValue());
				}

			} catch (IOException e) {
				LOG.warn("Failed to do replacement in file storage segment");
				throw new FileSystemSegmentException("Failed to do replacement in file storage segment", e);
			}

			LOG.debug("Successfully replaced");
			return true;
		}

		LOG.debug("Cannot perform replacement. Specified key is not in this segment");
		return false;
	}


	public boolean remove(final byte[] key) throws FileSystemSegmentException {
		LOG.debug("Starting to remove");

		if (contains(key)) {
			currentSize.set(0);

			try (final SegmentIterator segmentIterator = SegmentIterator.newInstance(fileLocation)) {
				replaceOutputStream(false);

				while (segmentIterator.hasNext()) {
					final KeyValuePair pair = segmentIterator.next();

					if (keysAreEqual(key, pair)) {
						continue;
					}

					write(pair.getSerializedKey(), pair.getSerializedValue());
				}

			} catch (IOException e) {
				LOG.warn("Failed to remove key-value pair in file storage segment");
				throw new FileSystemSegmentException("Failed to remove key-value pair in file storage segment", e);
			}

			LOG.debug("Successfully removed key-value pair");
			return true;
		}

		LOG.debug("Cannot remove. Specified key is not in this segment");
		return false;
	}


	public void clear() throws FileSystemSegmentException {
		try {
			replaceOutputStream(false);
			currentSize.set(0);
		} catch (IOException e) {
			LOG.warn("Failed to clear file storage segment");
			throw new FileSystemSegmentException("Failed to clear file storage segment", e);
		}
	}


	public boolean contains(final byte[] key) throws FileSystemSegmentException {
		try (final SegmentIterator segmentIterator = SegmentIterator.newInstance(fileLocation)) {

			while (segmentIterator.hasNext()) {
				final KeyValuePair pair = segmentIterator.next();

				if (pair.isPresent()) {
					if (keysAreEqual(key, pair)) {
						LOG.debug("Found a key. Specified key is located in this segment");
						return true;
					}
				}
			}

		} catch (IOException e) {
			LOG.warn("Failed to check if file system segment contains key", e);
			throw new FileSystemSegmentException("Failed to check if file system segment contains key", e);
		}

		LOG.debug("Haven't found a key. Specified key is not located in this segment");
		return false;
	}


	@Override
	public void close() throws FileSystemSegmentException {
		LOG.debug("Closing segment");

		try {
			outputStream.close();

			if (!isPersistent) {
				LOG.debug("Segment is not persistent, so starting to delete data completely");
				Files.delete(Paths.get(fileLocation));
			}

		} catch (IOException e) {
			LOG.warn("Failed to close file system segment", e);
			throw new FileSystemSegmentException("Failed to close file system segment", e);
		}

	}


	private void write(byte[] bytes) throws IOException {
		final ByteBuffer length = ByteBuffer.allocate(4);
		length.putInt(bytes.length);
		outputStream.write(length.array());	// write length to determine read size afterwards
		outputStream.write(bytes);
		outputStream.flush();
	}


	private boolean keysAreEqual(byte[] key, KeyValuePair pair) {
		return Arrays.equals(pair.getSerializedKey(), key);
	}


	private void replaceOutputStream(final boolean append) throws IOException {
		if (outputStream != null) {
			outputStream.close();
		}

		this.outputStream = new BufferedOutputStream(new FileOutputStream(fileLocation, append));
	}


	private void calculateMaxSize(final FileSystemConfiguration configuration) {
		final int size = configuration.getSize();
		switch(configuration.getSizeUnit()) {
			case KB:
				this.maxSize = (size * 1024) / configuration.getSegmentsAmount();
				break;

			case MB:
				this.maxSize = (size * (int) Math.pow(1024, 2)) / configuration.getSegmentsAmount();
				break;

			case GB:
				this.maxSize = (size * (int) Math.pow(1024, 3)) / configuration.getSegmentsAmount();
		}
	}



	private static final class SegmentIterator implements AutoCloseable {

		private BufferedInputStream stream;
		private int nextReadLength;
		private boolean isEof;


		private SegmentIterator(final String location) throws IOException {
			this.stream = new BufferedInputStream(new FileInputStream(location));
			moveCursor();	// to initial position
		}


		static SegmentIterator newInstance(final String location) throws IOException {
			return new SegmentIterator(location);
		}


		boolean hasNext() {
			return nextReadLength > 0 && !isEof;
		}


		KeyValuePair next() throws IOException {

			if (nextReadLength > 0 && !isEof) {
				// ---- read key
				final byte[] key = new byte[nextReadLength];
				stream.read(key);

				moveCursor();

				// ---- read value
				final byte[] value = new byte[nextReadLength];
				stream.read(value);
				moveCursor();

				return new KeyValuePair(key, value);
			}

			return KeyValuePair.empty();
		}


		@Override
		public void close() throws IOException {
			stream.close();
		}

		private void moveCursor() throws IOException {
			final byte[] size = new byte[4];
			final int readResult = stream.read(size);

			nextReadLength = ByteBuffer.wrap(size).getInt();

			if (readResult == -1) {
				isEof = true;
			}
		}


	}



}

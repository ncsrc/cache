package dev.test.cache.core.storage.impl.ram;

import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.holders.ValueHolder;

import java.util.Deque;
import java.util.concurrent.ConcurrentLinkedDeque;


public class FifoRamStorage<K, V> extends AbstractRamStorage<K, V> {

	// for quick FIFO eviction
	Deque<K> keyDeque;


	public FifoRamStorage(final RamConfiguration ramConfiguration) {
		super(ramConfiguration);
		this.keyDeque = new ConcurrentLinkedDeque<>();
	}


	@Override
	protected KeyValuePair<K, V> performEviction() {
		K evictedKey = keyDeque.peek();
		return remove(evictedKey);
	}


	@Override
	protected void doSave(final KeyValuePair<K, V> keyValuePair) {
		final ValueHolder possibleValue = data.putIfAbsent(keyValuePair.getKey(), new ValueHolder(keyValuePair.getSerializedValue()));
		if (possibleValue == null) {
			keyDeque.add(keyValuePair.getKey());
		}
	}


	@Override
	public byte[] get(K key) {
		LOG.debug("Getting value from RAM storage by key: {}", key);
		final ValueHolder valueHolder = data.get(key);

		if (valueHolder != null) {
			valueHolder.incrementHitCounter();
			return valueHolder.getSerializedValue();
		}

		// if miss
		return null;
	}


	@Override
	protected void afterRemove(K key) {
		keyDeque.remove(key);
	}


	@Override
	protected void afterClear() {
		keyDeque.clear();
	}


	@Override
	protected void afterClose() {
		keyDeque = null;
	}

}

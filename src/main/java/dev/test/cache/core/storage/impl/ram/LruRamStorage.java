package dev.test.cache.core.storage.impl.ram;

import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.holders.ValueHolder;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;


public class LruRamStorage<K, V> extends AbstractRamStorage<K, V> {

	// For moving last-queried keys, for quick LRU eviction
	List<K> keyList;


	public LruRamStorage(final RamConfiguration ramConfiguration) {
		super(ramConfiguration);
		this.keyList = Collections.synchronizedList(new LinkedList<>());
	}


	@Override
	protected KeyValuePair<K, V> performEviction() {
		K evictedKey = keyList.get(keyList.size() - 1);
		return remove(evictedKey);
	}


	@Override
	protected void doSave(final KeyValuePair<K, V> keyValuePair) {
		final ValueHolder possibleValue = data.putIfAbsent(keyValuePair.getKey(), new ValueHolder(keyValuePair.getSerializedValue()));
		if (possibleValue == null) {
			keyList.add(keyValuePair.getKey());
		}
	}


	@Override
	public byte[] get(K key) {
		LOG.debug("Getting value from RAM storage by key: {}", key);

		final ValueHolder valueHolder = data.get(key);
		if (valueHolder != null) {
			valueHolder.incrementHitCounter();
			swapKeys(key);
			return valueHolder.getSerializedValue();
		}

		// if miss
		return null;
	}


	@Override
	protected void afterRemove(K key) {
		keyList.remove(key);
	}


	@Override
	protected void afterClear() {
		keyList.clear();
	}


	@Override
	protected void afterClose() {
		keyList = null;
	}


	// Swaps 2 neighbouring keys, moves accessed key to the beginning of the list,
	// so frequently-queried keys will be at the beginning and LRU always in the end
	private void swapKeys(K key) {
		final int index = keyList.indexOf(key);

		if (index > 0) {
			Collections.swap(keyList, index, index - 1);
		}
	}


}

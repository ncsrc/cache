package dev.test.cache.core.storage.impl.ram;

import dev.test.cache.config.ram.RamConfiguration;
import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.RamStorage;
import dev.test.cache.core.storage.holders.ValueHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


abstract class AbstractRamStorage<K, V> implements RamStorage<K, V> {

	protected static final Logger LOG = LoggerFactory.getLogger(AbstractRamStorage.class);

	protected final RamConfiguration ramConfiguration;

	protected Map<K, ValueHolder> data;


	AbstractRamStorage(final RamConfiguration ramConfiguration) {
		this.ramConfiguration = ramConfiguration;
		this.data = new ConcurrentHashMap<>();
	}


	@Override
	public KeyValuePair<K, V> save(final KeyValuePair<K, V> keyValuePair) {
		LOG.debug("Starting to save value {} with key {} to RAM storage", keyValuePair.getValue(), keyValuePair.getKey());
		final KeyValuePair<K, V> evicted = evictIfCacheIsFull();
		doSave(keyValuePair);
		return evicted;
	}


	protected abstract void doSave(final KeyValuePair<K, V> keyValuePair);

	protected abstract KeyValuePair<K, V> performEviction();


	@Override
	public boolean replace(final KeyValuePair<K, V> keyValuePair) {
		LOG.debug("Starting to replace value {} with key: {} in RAM storage", keyValuePair.getValue(), keyValuePair.getKey());
		final ValueHolder replaced = data.replace(keyValuePair.getKey(), new ValueHolder(keyValuePair.getSerializedValue()));
		return isReplaced(replaced);
	}


	@Override
	public KeyValuePair<K, V> remove(K key) {
		LOG.debug("Removing value from RAM storage by key: {}", key);
		final ValueHolder removed = data.remove(key);

		if (removed == null) {
			LOG.debug("There is no value to remove by key: {}", key);
			return null;
		} else {
			afterRemove(key);
			return new KeyValuePair<>(key, removed.getSerializedValue());
		}
	}

	// For additional possible after-remove actions in subclasses
	protected abstract void afterRemove(K key);


	@Override
	public void clear() {
		LOG.debug("Starting to clear RAM cache");
		data.clear();
		afterClear();
	}

	// For additional clearing in subclasses
	protected abstract void afterClear();


	@Override
	public boolean contains(K key) {
		return data.containsKey(key);
	}


	// Blocking while closing
	@Override
	public synchronized void close() {
		LOG.info("Closing RAM cache");
		data = null;
		afterClose();
	}

	// For additional closing actions in subclasses
	protected abstract void afterClose();


	private KeyValuePair<K, V> evictIfCacheIsFull() {
		KeyValuePair<K, V> evicted = KeyValuePair.empty();
		if (data.size() >= ramConfiguration.getSize()) {
			LOG.debug("RAM cache has reached its maximum capacity. Starting to evict some element.");
			evicted = performEviction();
		}

		return evicted;
	}


	private boolean isReplaced(final ValueHolder replaced) {
		if (replaced != null) {
			return true;
		} else {
			LOG.debug("There is nothing to replace");
			return false;
		}
	}


}

package dev.test.cache.core.storage;

import dev.test.cache.core.storage.holders.KeyValuePair;
import dev.test.cache.core.storage.impl.filesystem.exceptions.FileSystemStorageException;


public interface FileSystemStorage<K, V> extends AutoCloseable {


	// Returns true if eviction was performed
	boolean save(KeyValuePair<K, V> keyValuePair) throws FileSystemStorageException;

	// Returns replaced key-value pair
	boolean replace(KeyValuePair<K, V> keyValuePair) throws FileSystemStorageException;

	byte[] get(byte[] key) throws FileSystemStorageException;

	// Returns removed value
	boolean remove(byte[] key) throws FileSystemStorageException;

	void clear() throws FileSystemStorageException;

	boolean contains(byte[] key) throws FileSystemStorageException;

	@Override
	void close() throws FileSystemStorageException;



}

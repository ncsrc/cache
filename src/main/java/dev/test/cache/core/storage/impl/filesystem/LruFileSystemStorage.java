package dev.test.cache.core.storage.impl.filesystem;

import dev.test.cache.config.filesystem.FileSystemConfiguration;

import java.util.Collections;


public class LruFileSystemStorage<K, V> extends AbstractFileSystemStorage<K, V> {



	public LruFileSystemStorage(final FileSystemConfiguration configuration, final String cacheId) {
		super(configuration, cacheId);
	}


	@Override
	protected FileStorageSegment performEviction() {
		pollLastSegment();
		return makeNewSegment();
	}


	@Override
	protected void afterReplace(FileStorageSegment segment) {
		swapSegments(segment);
	}


	@Override
	protected void afterGet(FileStorageSegment segment) {
		swapSegments(segment);
	}


	@Override
	protected void afterRemove(FileStorageSegment segment) {
		swapSegments(segment);
	}


	@Override
	protected void afterContains(FileStorageSegment segment) {
		swapSegments(segment);
	}


	private FileStorageSegment makeNewSegment() {
		final FileStorageSegment newSegment = new FileStorageSegment(rootDirectory, configuration);
		newSegment.tryLock();
		segments.add(newSegment);
		return newSegment;
	}


	// Last is always LRU
	private void pollLastSegment() {
		final FileStorageSegment lruSegment = segments.get(segments.size() - 1);
		segments.remove(lruSegment);
		fullSegments.remove(lruSegment);
		lruSegment.close();
	}


	// Swaps 2 neighbouring segments, moves accessed segments to the beginning of the list,
	// so frequently-queried segments will be at the beginning and LRU always in the end
	private void swapSegments(FileStorageSegment segment) {
		final int index = segments.indexOf(segment);

		if (index > 0) {
			Collections.swap(segments, index, index - 1);
		}
	}

}

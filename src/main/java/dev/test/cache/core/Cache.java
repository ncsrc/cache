package dev.test.cache.core;

import dev.test.cache.core.exceptions.CacheReadException;
import dev.test.cache.core.exceptions.CacheShutdownException;
import dev.test.cache.core.exceptions.CacheSystemException;
import dev.test.cache.core.exceptions.CacheWriteException;


public interface Cache<K, V> extends AutoCloseable {

	void put(K key, V value) throws CacheWriteException;

	boolean replace(K key, V value) throws CacheWriteException;

	V get(K key) throws CacheReadException;

	boolean contains(K key) throws CacheReadException;

	boolean remove(K key) throws CacheWriteException;

	void clear() throws CacheSystemException;

	@Override
	void close() throws CacheShutdownException;

}

package dev.test.cache.core.manager;

import dev.test.cache.core.Cache;


// Wrapper of Cache instance that also contains some metadata
class CacheHolder {

	private Cache<?, ?> cacheInstance;
	private Class<?> keyType;
	private Class<?> valueType;


	public CacheHolder(Cache<?, ?> cacheInstance, Class<?> keyType, Class<?> valueType) {
		this.cacheInstance = cacheInstance;
		this.keyType = keyType;
		this.valueType = valueType;
	}


	public Cache<?, ?> getCacheInstance() {
		return cacheInstance;
	}

	public Class<?> getKeyType() {
		return keyType;
	}

	public Class<?> getValueType() {
		return valueType;
	}


}

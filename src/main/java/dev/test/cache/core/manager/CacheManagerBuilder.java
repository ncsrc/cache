package dev.test.cache.core.manager;

import dev.test.cache.config.*;
import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.filesystem.SizeUnit;
import dev.test.cache.config.ram.RamConfiguration;

import java.io.File;


public class CacheManagerBuilder {

	// Default configurations
	private static final RamConfiguration DEFAULT_MEMORY_CONFIGURATION;

	private static final FileSystemConfiguration DEFAULT_FILE_SYSTEM_CONFIGURATION;

	private static final CachingStrategy DEFAULT_CACHING_STRATEGY;

	// Specific configurations
	private RamConfiguration ramConfiguration;

	private FileSystemConfiguration fileSystemConfiguration;

	private CachingStrategy cachingStrategy;


	// Static factory method for convenience
	public static CacheManagerBuilder newInstance() {
		return new CacheManagerBuilder();
	}


	public CacheManagerBuilder withMemoryConfiguration(final RamConfiguration ramConfiguration) {
		this.ramConfiguration = ramConfiguration;
		return this;
	}


	public CacheManagerBuilder withFileSystemConfiguration(final FileSystemConfiguration fileSystemConfiguration) {
		this.fileSystemConfiguration = fileSystemConfiguration;
		return this;
	}


	public CacheManagerBuilder withCachingStrategy(final CachingStrategy cachingStrategy) {
		this.cachingStrategy = cachingStrategy;
		return this;
	}


	public CacheManager build() throws InvalidConfigurationException {
		final CacheConfiguration configuration = getCacheConfiguration();
		validateConfiguration(configuration);
		return new CacheManager(configuration);
	}


	private void validateConfiguration(CacheConfiguration configuration) {
		if (configuration.getRamConfiguration().getSize() < 0) {
			throw new InvalidConfigurationException("Size of RAM cache is less than 0");
		}

		if (configuration.getFileSystemConfiguration().getSegmentsAmount() < 0) {
			throw new InvalidConfigurationException("Size of FS segments is less than 0");
		}

		if (configuration.getFileSystemConfiguration().getSize() < 0) {
			throw new InvalidConfigurationException("Size of FS cache is less than 0");
		}
	}


	private CacheConfiguration getCacheConfiguration() {
		final RamConfiguration ramConfiguration =
				(this.ramConfiguration == null) ? DEFAULT_MEMORY_CONFIGURATION : this.ramConfiguration;

		final FileSystemConfiguration fileSystemConfiguration =
				(this.fileSystemConfiguration == null) ? DEFAULT_FILE_SYSTEM_CONFIGURATION : this.fileSystemConfiguration;

		final CachingStrategy cachingStrategy = getCachingStrategy(fileSystemConfiguration.isPersistent());

		return new CacheConfiguration(ramConfiguration, fileSystemConfiguration, cachingStrategy);
	}


	private CachingStrategy getCachingStrategy(final boolean isPersistent) {
		final CachingStrategy cachingStrategy;

		if (isPersistent) {
			cachingStrategy = CachingStrategy.WRITE_THROUGH;
		} else {
			cachingStrategy = (this.cachingStrategy == null) ? DEFAULT_CACHING_STRATEGY : this.cachingStrategy;
		}

		return cachingStrategy;
	}


	// 2d option: ./cache
	private static String getDefaultFileSystemCacheLocation() {
		return System.getProperty("java.io.tmpdir") + File.separator + "cache";
	}


	static {
		DEFAULT_MEMORY_CONFIGURATION = new RamConfiguration(100, EvictionPolicy.LRU);

		DEFAULT_FILE_SYSTEM_CONFIGURATION = new FileSystemConfiguration(
				getDefaultFileSystemCacheLocation(),
				10, SizeUnit.MB,
				false,
				EvictionPolicy.LRU,
				4
		);

		DEFAULT_CACHING_STRATEGY = CachingStrategy.WRITE_BACK;
	}

}

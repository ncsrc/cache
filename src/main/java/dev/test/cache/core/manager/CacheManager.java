package dev.test.cache.core.manager;

import dev.test.cache.config.CacheConfiguration;
import dev.test.cache.core.Cache;
import dev.test.cache.core.cacheimpl.WriteBackCache;
import dev.test.cache.core.cacheimpl.WriteThroughCache;
import dev.test.cache.core.exceptions.CacheInitializationException;
import dev.test.cache.core.exceptions.CacheShutdownException;
import dev.test.cache.core.exceptions.CacheSystemException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


public class CacheManager implements AutoCloseable {

	private static final Logger LOG = LoggerFactory.getLogger(CacheManager.class);

	private final Map<String, CacheHolder> caches = new ConcurrentHashMap<>();

	private CacheConfiguration cacheConfiguration;


	public CacheManager(final CacheConfiguration cacheConfiguration) {
		this.cacheConfiguration = cacheConfiguration;
	}


	// Classes for type safety and correct casting
	@SuppressWarnings("unchecked")
	public <K, V> Cache<K, V> getCache(final String name, final Class<K> keyType, final Class<V> valueType) throws CacheSystemException {
		final CacheHolder cacheHolder = caches.get(name);
		checkTypes(keyType, valueType, cacheHolder);
		return (Cache<K, V>) cacheHolder.getCacheInstance();
	}


	// Using existing configuration
	public synchronized <K, V> Cache<K, V> makeCache(final String name, final Class<K> keyType, final Class<V> valueType) throws CacheInitializationException {
		LOG.debug("Starting to make new cache {}", name);
		checkCacheIsNotExist(name);

		final Cache<K, V> cache = CacheFactory.makeCache(cacheConfiguration, name);
		caches.put(name, new CacheHolder(cache, keyType, valueType));

		return cache;
	}


	public void removeCache(final String name) throws CacheShutdownException {
		final CacheHolder removedCache = caches.remove(name);

		if (removedCache != null) {
			removedCache.getCacheInstance().close();
		}

	}


	@Override
	public void close() throws CacheShutdownException {
		caches.forEach((name, cache) -> cache.getCacheInstance().close());
		caches.clear();
	}


	private <K, V> void checkTypes(Class<K> keyType, Class<V> valueType, CacheHolder cacheHolder) {
		if (!cacheHolder.getKeyType().equals(keyType) || !cacheHolder.getValueType().equals(valueType)) {
			throw new CacheSystemException("Specified key or value type is not the same as in actual cache instance");
		}
	}


	private void checkCacheIsNotExist(final String name) {
		if (caches.containsKey(name)) {
			LOG.warn("Cache {} already exists", name);
			throw new CacheSystemException("Cache with specified name already exists");
		}
	}



	// to bury ugly switch
	private static final class CacheFactory {

		static synchronized <K, V> Cache<K, V> makeCache(final CacheConfiguration cacheConfiguration, final String name) {

			// needed to uniquely identify file system storage and restore afterwards if it is persistent
			final String cacheId = Integer.toHexString(name.hashCode());

			switch(cacheConfiguration.getCachingStrategy()) {
				case WRITE_THROUGH:
					return new WriteThroughCache<>(cacheConfiguration, cacheId);
				case WRITE_BACK:
					return new WriteBackCache<>(cacheConfiguration, cacheId);
				default:
					return new WriteBackCache<>(cacheConfiguration, cacheId);
			}

		}

	}

}

package dev.test.cache.core.exceptions;


public class CacheWriteException extends CacheSystemException {

	public CacheWriteException() {
		super();
	}

	public CacheWriteException(String message) {
		super(message);
	}

	public CacheWriteException(String message, Throwable cause) {
		super(message, cause);
	}

}

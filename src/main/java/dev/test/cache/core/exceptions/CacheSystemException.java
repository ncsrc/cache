package dev.test.cache.core.exceptions;


// root in exceptions hierarchy
public class CacheSystemException extends RuntimeException {


	public CacheSystemException() {
		super();
	}

	public CacheSystemException(String message) {
		super(message);
	}

	public CacheSystemException(String message, Throwable cause) {
		super(message, cause);
	}



}

package dev.test.cache.core.exceptions;


public class CacheReadException extends CacheSystemException {

	public CacheReadException() {
		super();
	}

	public CacheReadException(String message) {
		super(message);
	}

	public CacheReadException(String message, Throwable cause) {
		super(message, cause);
	}

}

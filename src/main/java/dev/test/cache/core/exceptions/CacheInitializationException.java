package dev.test.cache.core.exceptions;


public class CacheInitializationException extends CacheSystemException {

	public CacheInitializationException() {
		super();
	}

	public CacheInitializationException(String message) {
		super(message);
	}

	public CacheInitializationException(String message, Throwable cause) {
		super(message, cause);
	}

}

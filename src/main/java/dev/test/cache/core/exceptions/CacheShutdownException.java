package dev.test.cache.core.exceptions;


public class CacheShutdownException extends CacheSystemException {

	public CacheShutdownException() {
		super();
	}

	public CacheShutdownException(String message) {
		super(message);
	}

	public CacheShutdownException(String message, Throwable cause) {
		super(message, cause);
	}

}

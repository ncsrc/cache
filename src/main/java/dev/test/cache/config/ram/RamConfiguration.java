package dev.test.cache.config.ram;

import dev.test.cache.config.EvictionPolicy;


public class RamConfiguration {

	private final int size;

	private final EvictionPolicy evictionPolicy;


	public RamConfiguration(int size, EvictionPolicy evictionPolicy) {
		this.size = size;
		this.evictionPolicy = evictionPolicy;
	}


	public int getSize() {
		return size;
	}

	public EvictionPolicy getEvictionPolicy() {
		return evictionPolicy;
	}

}

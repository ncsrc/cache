package dev.test.cache.config.filesystem;

import dev.test.cache.config.EvictionPolicy;


public class FileSystemConfiguration {

	private final String location;

	private final int size;

	private final SizeUnit sizeUnit;

	private final boolean isPersistent;

	private final EvictionPolicy evictionPolicy;

	private final int segmentsAmount;


	public FileSystemConfiguration(String location, int size, SizeUnit sizeUnit, boolean isPersistent, EvictionPolicy evictionPolicy, int segmentsAmount) {
		this.location = location;
		this.size = size;
		this.sizeUnit = sizeUnit;
		this.isPersistent = isPersistent;
		this.evictionPolicy = evictionPolicy;
		this.segmentsAmount = segmentsAmount;
	}


	public String getLocation() {
		return location;
	}

	public int getSize() {
		return size;
	}

	public SizeUnit getSizeUnit() {
		return sizeUnit;
	}

	public boolean isPersistent() {
		return isPersistent;
	}

	public EvictionPolicy getEvictionPolicy() {
		return evictionPolicy;
	}


	public int getSegmentsAmount() {
		return segmentsAmount;
	}


}


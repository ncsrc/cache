package dev.test.cache.config;

import dev.test.cache.config.filesystem.FileSystemConfiguration;
import dev.test.cache.config.ram.RamConfiguration;


public class CacheConfiguration {

	private final RamConfiguration ramConfiguration;

	private final FileSystemConfiguration fileSystemConfiguration;

	private final CachingStrategy cachingStrategy;


	public CacheConfiguration(RamConfiguration ramConfiguration, FileSystemConfiguration fileSystemConfiguration, CachingStrategy cachingStrategy) {
		this.ramConfiguration = ramConfiguration;
		this.fileSystemConfiguration = fileSystemConfiguration;
		this.cachingStrategy = cachingStrategy;
	}


	public RamConfiguration getRamConfiguration() {
		return ramConfiguration;
	}


	public FileSystemConfiguration getFileSystemConfiguration() {
		return fileSystemConfiguration;
	}


	public CachingStrategy getCachingStrategy() {
		return cachingStrategy;
	}


}
